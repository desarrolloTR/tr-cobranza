<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\NotesController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\TemporalGenerationController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\PaymentRunsController;
use App\Http\Controllers\FileUploadController;
use App\Models\Student;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [AuthController::class, 'login'])->middleware('cors');
Route::post('registrar', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('perfil', [AuthController::class, 'me'])->middleware('cors');
    Route::put('confirmar-sms/{id}', [AuthController::class, 'confirmCodeAccess']);

    //RUTA QUE CIERRA LA SESIÓN
    Route::post('logout', [AuthController::class, 'logout']);

    //CRUD QUE PERTENECE AL REGISTRO DE GENERACIONES
    Route::post('registrar-generacion', [TemporalGenerationController::class, 'registerGeneration']);
    Route::get('generacion/{id}', [TemporalGenerationController::class, 'getGeneration']);
    Route::get('generaciones', [TemporalGenerationController::class, 'getGenerations']);
    Route::put('generacion/{id}', [TemporalGenerationController::class, 'updateGeneration']);
    Route::get('generaciones-activas', [TemporalGenerationController::class, 'getGenerationActive']);
    Route::get('generaciones-inactivas', [TemporalGenerationController::class, 'getGenerationInactive']);

    //CRUD QUE PERTENECE AL USUARIO
    Route::get('estudiantes', [StudentController::class, 'getStudents']);
    Route::get('estudiante/{id}', [StudentController::class, 'getStudent']);
    Route::post('registrar-estudiante', [StudentController::class, 'registerStudent']);
    Route::put('estudiante/{id}', [StudentController::class, 'updateStudent']);
    Route::get('estudiante-generacion/{id}', [StudentController::class, 'getStudentForCategory']);
    Route::get('estudiante-notas/{id}', [StudentController::class, 'getStudentForComment']);
    Route::get('estudiantes-activos', [StudentController::class, 'getStudentsActive']);
    Route::get('estudiantes-inactivos', [StudentController::class, 'getStudentInactive']);

    //CRUD QUE PERTENCE AL PROCEDIMINETO DE PAGOS
    Route::get('pagos', [PaymentsController::class, 'getPayments']);
    Route::get('pago/{id}', [PaymentsController::class, 'getPayment']);


    // Registrar corrida
    Route::post('registrar-corrida', [PaymentRunsController::class, 'registerPaymentRun']);
    //Consulta corridas
    Route::get('corridas', [PaymentRunsController::class, 'getPaymentRuns']);
    // Consulta corridas de un usuario
    Route::get('corridas-usuario/{id}', [PaymentRunsController::class, 'getUserPayments']);
    // Obtiene datos de un pago
    Route::get('obtener-pago/{id}', [PaymentsController::class, 'getOnePayment']);
    // Modifica pago / Realiza pago
    Route::put('actualizar-pago/{id}', [PaymentsController::class, 'updatePayment']);
    // Actualiza monto restante
    Route::put('actualiza-total/{id}', [PaymentRunsController::class, 'updateOutstanding']);
    // obtiene información de la corrida y el cliente
    Route::get('getOnePaymentRun/{id}', [PaymentRunsController::class, 'getOnePaymentRun']);
    // Registra un nuevo pago
    Route::post('registrar-pago', [PaymentsController::class, 'RegisterPayments']);
    // Metodo para actualizar cantidad entre los restantes
    Route::put('actualiza-restantes/{id}', [PaymentsController::class, 'sumPayments']);
    // Metodo para actualizar cantidad en el siguiente pago
    Route::put('actualiza-siguiente/{id}', [PaymentsController::class, 'sumNextPayment']);

    Route::post('uploading-file', [FileUploadController::class, 'upload']);


    //CRUD QUE PERTENCE AL REGISTRO DE CUENTAS BANCARIAS
    Route::post('registrar-cuenta', [BankController::class, 'registerBank']);
    Route::get('cuentas', [BankController::class, 'getAccountsBank']);
    Route::get('cuenta/{id}', [BankController::class, 'getAccountBank']);
    Route::put('cuenta/{id}', [BankController::class, 'updateAccountBank']);
    Route::delete('cuenta/{id}', [BankController::class, 'deleteAccountBank']);


    //CRUD QUE PERTENECE AL REGISTRO DE NOTAS POR USUARIO
    Route::post('registrar-nota', [NotesController::class, 'registerNote']);
    Route::get('notas', [NotesController::class, 'getNotes']);
    Route::get('nota/{id}', [NotesController::class, 'getNote']);
    Route::put('nota/{id}', [NotesController::class, 'updateNote']);
    route::delete('notas/{id}', [NotesController::class, 'deleteNotes']);


    //RUTA QUE IMPRIME A LA IMPRESORA DE TICKETS
    Route::post('imprimir-ticket', [PaymentsController::class, 'printfTicket']);

    //RUTAS PARA IMPLEMENTAR EN EL DASHBOARD PRINCIPAL
    Route::get('ultimos-pagos-meses', [PaymentRunsController::class, 'lastTwoMonthsPayments']);
    Route::get('ultimos-pagos-quincena', [PaymentRunsController::class, 'lastThreeMonthsPayments']);
    Route::get('pagos-proximos', [PaymentRunsController::class, 'paymentsComingSoon']);
    Route::get('pagos-notificaciones', [PaymentRunsController::class, 'paymentsNotifications']);
    Route::put('actualiza-notificaciones/{id}', [PaymentRunsController::class, 'updateNotification']);
    Route::get('acomulativo', [PaymentRunsController::class, 'accruedIncome']);


});






