<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $getDay =  date('Y-m-d H:i:s');
        $dateFakeInformationPayment = array(
            array('payment_id' => 1,
            'consultant' => 'Asteci',
            'client' => 'grupo salindas',
            'date_entry' => $getDay,
            'schema' => 'mixto',
            'amount_receive' => '27000.00',
            'amount_payment' => '67000.00',
            'outstanding' => '67000.00',
            'created_at' => Carbon::now())
        );

        DB::table('information_payment')->insert($dateFakeInformationPayment);
    }
}
