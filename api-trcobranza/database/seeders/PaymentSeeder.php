<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $getDay =  date('Y-m-d H:i:s');
        $dataFakePayment = array(
            array(
                'number_payment' => '2',
                'date_payment' => $getDay,
                'quantity' => '5500',
                'status' => 1,
                'method_payment' => 'efectivo',
                'created_at' => Carbon::now()),
        );

        DB::table('payments')->insert($dataFakePayment);
    }
}
