<?php

namespace Database\Seeders;


use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'name' => 'Roberto Jara Ramírez',
                'email' => 'devjara16@gmail.com',
                'password' => Hash::make('password'),
                'status' => 0,
                'code_access' => '23411',
                'created_at' => Carbon::now()
            ),
            array(
                'id' => 2,
                'name' => 'Sergio Pena Hernandez',
                'email' => 'pena@hotmail.com',
                'password' => Hash::make('password'),
                'status' => 0,
                'code_access' => '80923',
                'created_at' => Carbon::now()
            )
        );

        User::insert($data);
    }


}
