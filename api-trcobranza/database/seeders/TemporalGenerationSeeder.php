<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemporalGenerationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataFake = array(
            array('user_id' => 1, 'name_generation' => 'Java 29/01/2022', 'technology' => 'Java', 'created_at' => Carbon::now()),
            array('user_id' => 2, 'name_generation' => 'Net 29/01/2022', 'technology' => '.Net', 'created_at' =>  Carbon::now()),
            array('user_id' => 2, 'name_generation' => 'Net 20/01/2022', 'technology' => '.Net', 'created_at' => Carbon::now())
        );

        DB::table('temporal_generation')->insert($dataFake);
    }
}
