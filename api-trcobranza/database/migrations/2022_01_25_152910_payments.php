<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('infoPayment_id');
            $table->unsignedBigInteger('user_id');
            $table->string('number_payment',10);
            $table->date('date_payment');
            $table->integer('quantity');
            $table->tinyInteger('status');
            $table->string('method_payment',40)->nullable();
            $table->integer('discount')->nullable();
            $table->integer('discount_quantity')->nullable();
            $table->string('description',100)->nullable();
            $table->boolean('is_read')->nullable();
            $table->timestamps();
        });

        Schema::table('payments', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('infoPayment_id')->references('id')->on('information_payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
