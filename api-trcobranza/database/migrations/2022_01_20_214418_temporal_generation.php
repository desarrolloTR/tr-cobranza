<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TemporalGeneration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * !!Status 1 = activo 2 = inactivo
     */
    public function up()
    {
        Schema::create('temporal_generation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name_generation');
            $table->string('technology');
            $table->tinyInteger('status')->default(1)->nullable();
            $table->timestamps();
        });

        Schema::table('temporal_generation', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporal_generation');
    }
}
