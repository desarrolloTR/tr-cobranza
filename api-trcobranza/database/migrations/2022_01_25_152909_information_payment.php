<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InformationPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_payment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_id');
            $table->string('consultant',100);
            $table->string('client',100);
            $table->date('date_entry');
            $table->string('payment_period');
            $table->string('schema',20);
            $table->integer('amount_receive' ); //Monto a Persevir
            $table->integer('amount_payment'); //Monto a Pagar
            $table->integer('outstanding')->nullable(); //Monto Pendiente
            $table->timestamps();
        });
        Schema::table('information_payment', function(Blueprint $table){
            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_payment');
    }
}
