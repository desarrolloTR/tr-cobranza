<?php

namespace App\Http\Traits;


trait ResponsesJson
{
    public function successRegisterData($data, $message)
    {
        return response()->json(['status' => true, 'data' => $data, 'message' => $message], 201);
    }

    public function notFoundData($message)
    {
        return response()->json(['status' => false, 'message' => $message], 204);
    }

    public function updateData($message)
    {
        return response()->json(['status' => true, 'message' => $message]);
    }

    public function listData($data)
    {
        return response()->json(['status' => true, 'data' => $data],200);
    }

    public function exceptionErrorData($message, $e)
    {
        return response()->json([$message, $e],400);
    }

    public function generalMessages($status, $message, $code)
    {
        return response()->json([$status, $message], $code);
    }

    //
}
