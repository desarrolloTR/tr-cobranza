<?php

namespace App\Http\Controllers\Auth;

use Tymon;
use Exception;
use Carbon\Carbon;
use App\Models\User;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use App\Http\Requests\SMSRequest;
use App\Http\Utils\UNumberRandoms;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class AuthController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:api', ['except' => ['login']]);
    // }

    /**
    * *Función que registra un usuario
    */
    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->telephone = $request->input('telephone');
            $user->status = 0;
            $user->created_at = Carbon::now();
            $user->updated_at = null;
            $user->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $user]);
        }catch(Exception $e) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al registrar el usuario'. $e],500);
        }
    }


    public function login(LoginRequest $request)
    {
        $access = request(['email','password']);
        try {
            if(! $token = JWTAuth::attempt($access)) {
                return response()->json(['status' => false, 'message' => 'Email o contraseña incorrectos'],401);
            }
        }catch(JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Could Not Create Token'],500);
        }

        // $twilioSMS = $this->configurationTwilio();
        $numberAccess = UNumberRandoms::generateNumberRandom();
        // $message = $twilioSMS->messages
        // ->create(
        //     '+522231143871', // to
        //     [
        //         'from' => env('TWILIO_NUMBER'),
        //         'body' => 'Su código de acceso es: ' . '' . $numberAccess
        //     ]
        // );

        $session = Auth::user()->id;
        $userLogin = User::find($session);
        DB::beginTransaction();
        try {
            $userLogin->code_access = $numberAccess;
            $userLogin->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ocurrio un error al enviar el código de confirmaciónn'],500);
        }

        return response()->json(['status' => true, 'token' => $token, 'id' => Auth::user()->id, 'message' => 'Se ha enviado el código de acceso'],200);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     **Función que cierra sesión
     */
    public function logout(Request $request)
    {
        auth()->logout();
        return response()->json(['status' => true, 'message' => 'Se ha cerrado sesión correctamente'],200);
    }

    /**
     * *Función que obtiene el SID y TOKEN desde el archivo
     * !SID y TOKEN se obtiene desde el ENV.
     */

    private function configurationTwilio()
    {
        $twilio = new Client(env("TWILIO_SID"), env("TWILIO_TOKEN"));
        return $twilio;
    }

    private function getCodeSMS($id)
    {
        $getCodeOfConfirmation = User::select('code_access')->where('id', $id)->get();
        return $getCodeOfConfirmation;
    }

    public function confirmCodeAccess(SMSRequest $request, $id)
    {
        $smsUpdate = User::find($id);
        $codeAccess = $this->getCodeSMS($id);

        foreach($codeAccess as $code) {
            $access = $code->code_access;
        }

        DB::beginTransaction();
        try {
            $smsUpdate->confirm_code_access = $request->input('confirm_code_access');

            if($smsUpdate->confirm_code_access != $access) {

                return response(['status' => false, 'message' => 'El código de acceso es incorrecto'], 403);
            } else {
                $smsUpdate->status = 1;
                $smsUpdate->save();
                DB::commit();
                return response(['status' => true, 'message' => 'Inicio de sesión exitoso'],200);
            }

        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al actualizar el sms'. $e], 500);
        }
    }

}
