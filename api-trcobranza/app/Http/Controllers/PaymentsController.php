<?php
namespace App\Http\Controllers;

use Exception;
use App\Models\Student;
use App\Models\Payments;
use Mike42\Escpos\Printer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Mike42\Escpos\EscposImage;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PaymentRequest;
use App\Notifications\PaymentNotification;
use Illuminate\Support\Facades\Notification;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

class PaymentsController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    // Metodo para registrar un nuevo pago con su numero automatico
    public function registerPayments(PaymentRequest $request)
    {
        $newNumber = Payments::where('infoPayment_id', '=', $request->input('id'))->count() + 1;
        DB::beginTransaction();
        try {
            $payments = new Payments;
            $payments->infoPayment_id = $request->input('id');
            $payments->user_id  = 1;
            $payments->number_payment = $newNumber;
            $payments->date_payment = $request->input('date_payment');
            $payments->quantity = $request->input('quantity');
            $payments->status = 0;
            $payments->is_read = 0;
            $payments->created_at = Carbon::now();
            $payments->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $payments, 'message' => 'Pago registrado correctamente']);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrrido al registrar el pago'. $e]);
        }
    }
// Funcion que obtiene todos los pagos
    public function getPayments()
    {
        $payments = Payments::all();
        return response()->json(['status' => true, 'data' => $payments], 200);
    }
    // Selecciona pagos de una corrida
    public function getPayment($id)
    {
        $detailPayment = Payments::where('infoPayment_id', '=', $id)->get();
        return response()->json(['status' => true, 'data' => $detailPayment]);
    }
    // Funcion para agregar el total en los pagos restantes
    // El id que se solicita es el id de la corrida
    public function sumPayments(Request $request, $id)
    {
        // Hacemos un contador para saber entre cuantos pagos se dividira
        $counter = Payments::where('infoPayment_id', '=', $id)->where('status', '=', 0)->count();
        // Se divide el monto recibido entre el N numero de pagos
        $amountToSume = $request->input('amount') / $counter;
        try{
            // Se obtienen los pagos que serán modificados
            $payment = Payments::where('infoPayment_id', '=', $id)->where('status', '=', 0)->get();
            // Se convierte a arreglo para popder hacer el query en un foreach
            $arrayOf = json_decode(json_encode($payment), true);
            foreach ($arrayOf as $pay) {
                // Se necesita el foreach para poder acceder a la cantidad original del pago a modificar y asi modificarlos todos
                $paymentUpdate = Payments::where('infoPayment_id', '=', $id)->where('status', '=', 0)->update(['quantity' => $pay['quantity'] + $amountToSume]);
            }
            return response()->json(['status' => true, 'data' => $paymentUpdate, 'message' => 'Pagos actualizados correctamente']);
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => true, 'message' => 'Ocurrio un error al actualizar los pagos', $e], 500);
        }
    }
    // Funcion para agregar el total en el siguiente pago
    // El id que se solicita es el id de la corrida
    public function sumNextPayment(Request $request, $id)
    {
        try{
            // Se obtiene el pago siguiente donde el numero de pago sea el siguiente al proveido
            $payment = Payments::where('infoPayment_id', '=', $id)->where('number_payment', '=', $request->input('NPayment') + 1)->firstOrFail();
            $payment->quantity = $payment->quantity + $request->input('amount');
            $payment->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $payment, 'message' => 'Pagos actualizados correctamente']);
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => true, 'message' => 'El siguiente pago no existe', $e], 500);
        }
    }
    // Selecciona un pago en especifico
    public function getOnePayment($id)
    {
        $detailPayment = Payments::where('id', '=', $id)->get();
        return response()->json(['status' => true, 'data' => $detailPayment]);
    }

    public function updatePayment(PaymentRequest $request, $id)
    {
        $payment = Payments::findOrFail($id);
        try{
            $payment->quantity = $request->input('quantity');
            $payment->status = $request->input('status');
            $payment->method_payment = $request->input('method_payment');
            $payment->discount = $request->input('discount');
            $payment->discount_quantity = $request->input('discount_quantity');
            $payment->description = $request->input('description');
            $payment->updated_at = Carbon::now();
            $payment->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $payment, 'message' => 'El pago se ha actualizado'],201);
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => true, 'message' => 'Ocurrio un error al actualizar el pago', $e], 500);
        }
    }

    public function printfTicket(Request $request)
    {

        $namePrintf = "EPSON-TM30";
        $connector = new WindowsPrintConnector($namePrintf);
        $printer = new Printer($connector);

        //!!Logotipo
        //$logotype = EscposImage::load(dirname(__FILE__)."/resources/images/logotipo-ticket.png", false);
        //$printer->graphics($logotype);


        //!!Cabecera del ticket
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setTextSize(1, 1);
        $printer->text("================================================");
        $printer->text("TR Network\n");
        $printer->text("Número: ".$request->input('number_one')."\n");
        //!!=================


        //!!Cuerpo del ticket
        $printer->text("================================================");
        $printer->text("Fecha: ".date('d/m/y')."\n");
        $printer->text("\n");
        $printer->text("Recibimos de:".$request->input('received')."\n");
        $printer->setPrintLeftMargin(2);
        $printer->text("Número".str_repeat(" ", 38).$request->input('number_two')."\n");
        $printer->text("Importe:".str_repeat(" ", 28)."$ ". $request->input('import'). "\n");
        $printer->text("Deuda Previa:".str_repeat(" ", 23)."$ ". $request->input('remaining_prev'). "\n");
        $printer->text("Deuda Restante:".str_repeat(" ", 20)." $ ". $request->input('remaining_debt') . "\n");
        $printer->text("\n");
        //!!=================

        $printer->text("===============================================");
        $printer->text("Total:".str_repeat(" ", 30)."$".$request->input('total')."\n");
        $printer->text("===============================================");
        $printer->text("Descripción:"."\n");
        $printer->text($request->input('description'));
        $printer->setTextSize(2, 2);
        $printer->feed(5);
        $printer->feed(5);
        $printer->close();

        $email = Student::where('id', $request->id)->first();
        Student::find($request->id)->notify(new PaymentNotification($email));
    }

    private function sendInformationToPayment($email)
    {

        $infoMailPayment  = Notification::route('email',$email)->notify(new PaymentNotification($email));
        return $infoMailPayment;
    }


}
