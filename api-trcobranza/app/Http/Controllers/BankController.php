<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Bank;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BankRequest;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function registerBank(BankRequest $request)
    {
        DB::beginTransaction();
        try {
            $account = new Bank;
            $account->account_bank = $request->input('account_bank');
            $account->name = $request->input('name');
            $account->bank = $request->input('bank');
            $account->clabe = $request->input('clabe');
            $account->created_at = Carbon::now();
            $account->updated_at = null;
            $account->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $account, 'message' => 'Se registrado el número de cuenta correctamente'],201);

        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ocurrio un error al registrar el número de cuenta'.$e], 500);
        }
    }

    public function getAccountsBank()
    {
        $getAccount = Bank::all()->where('deleted_at','=', null);

        if(count($getAccount) < 0){
            return response()->json(['status' => false, 'message' => 'No existen datos']);
        }

        return response()->json(['status' => true, 'data' => $getAccount], 200);
    }

    public function getAccountBank($id)
    {
        $account = Bank::findOrFail($id);
        return response()->json(['status' => true, 'data' => $account], 200);
    }

    public function updateAccountBank(BankRequest $request, $id)
    {

        DB::beginTransaction();
        $updateAccount = Bank::findOrFail($id);
        try{
            $updateAccount->account_bank = $request->input('account_bank');
            $updateAccount->name = $request->input('name');
            $updateAccount->bank = $request->input('bank');
            $updateAccount->clabe = $request->input('clabe');
            $updateAccount->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $updateAccount, 'message' => 'Se actualizo correctamente el número de cuenta']);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ocurrio un error al actualizar los datos del número de cuenta']);
        }
    }

    public function deleteAccountBank($id)
    {
        $account = Bank::findOrFail($id);
        $account->delete();
        return response()->json(['status' => true, 'message' => 'La cuenta bancaria se ha eliminado correctamente'], 200);

    }

}
