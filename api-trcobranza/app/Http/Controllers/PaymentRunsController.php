<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use App\Models\Payments;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\PaymentInformation;
use App\Http\Utils\UCalculateAmounts;
use App\Http\Requests\PaymentRunsRequest;

class PaymentRunsController extends Controller
{
    // Funcion que obtiene todas las corridas
    public function getPaymentRuns() {
        $run = PaymentInformation::all();
        return response()->json(['status' => true, 'data' => $run],200);
    }
    // Selecciona una corrida en especifico
    public function getOnePaymentRun($id)
    {
        $detailPayment = PaymentInformation::where('information_payment.id', '=', $id)
        ->join('students as b', 'b.id', 'information_payment.student_id')
        ->get();
        return response()->json(['status' => true, 'data' => $detailPayment]);
    }
    // Funcion para obtener todas las corridas de un usuario
    public function getUserPayments($id)
    {
        $paymentRun = PaymentInformation::where('student_id', '=', $id)->get();
        return response()->json(['status' => true, 'data' => $paymentRun]);
    }
    // Funcion para registrar una nueva corrida
    public function registerPaymentRun(PaymentRunsRequest $request) {
        // Se transforma la fecha proveida a datetime para ser usada en los utils
        $fechaInicio = new Datetime($request->input('date_entry'));
        // Util para obtener cantidad total que pagara el cliente
        $totalAmount = UCalculateAmounts::calculateTotalAmountPay($request->input('amount_receive'));
        DB::beginTransaction();
        try {
            $run = new PaymentInformation;
            $run->student_id = $request->input('student_id');
            $run->consultant = $request->input('consultant');
            $run->client = $request->input('client');
            $run->date_entry = $request->input('date_entry');
            $run->payment_period = $request->input('payment_period');
            $run->schema = $request->input('schema');
            $run->amount_receive = $request->input('amount_receive');
            $run->amount_payment = $totalAmount;
            $run->outstanding = $totalAmount;
            $run->created_at = Carbon::now();
            $run->updated_at = NULL;
            $run->save();
            $id = $run->id;
            DB::commit();
            $this->makePayments($totalAmount, $fechaInicio, $request->input('payment_period'), $id);
            return response()->json(['status' => true, 'data' => $run, 'message' => 'Corrida creada correctamente'],201);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al registrar su corrida', $e], 500);
        }
    }
    // Actualiza el monto pendiente
    public function updateOutstanding(PaymentRunsRequest $request, $id)
    {
        $payment = PaymentInformation::findOrFail($id);
        try{
            if ($request->input('operation') === 0 ) {
                $payment->outstanding = $payment->outstanding + $request->input('outstanding');
            }
            else{ $payment->outstanding = $payment->outstanding - $request->input('outstanding'); }

            $payment->updated_at = Carbon::now();
            $payment->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $payment, 'message' => 'La corrida se ha actualizado'],201);
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => true, 'message' => 'Ocurrio un error al actualizar la corrida', $e], 500);
        }
    }
    // Funcion que crea los pagos que se realizaran
    public function makePayments($totalAmount, $entryDate, $period, $id)
    {
        if ($period === 'Quincenal') {
            $paymentDates = UCalculateAmounts::generateDatePaymentByFortnights($entryDate);
            $costPayment = UCalculateAmounts::calculateUnitPaymentForFortnights($totalAmount);
        }
        elseif ($period === 'Mensual') {
            $costPayment = UCalculateAmounts::calculateUnitPaymentPerMonth($totalAmount);
            $paymentDates = UCalculateAmounts::generateDatePaymentByMonth($entryDate);
        }
        // Array para insertar los pagos de la corrida
        $count = 1;
        foreach($paymentDates as $key) {
            $data = array('infoPayment_id' => $id, 'user_id' => '1', 'number_payment' => $count, 'date_payment' => $key, 'quantity' => $costPayment
            , 'status' => '0', 'discount_quantity' => '0', 'is_read' => '0','created_at' => Carbon::now());
            Payments::insert($data);
            $count++;
        }
    }


    public function lastTwoMonthsPayments()
    {
        $getLastPayments = DB::table('payments as a')
            ->join('information_payment as b', 'b.id', 'a.infoPayment_id')
            ->join('students as c', 'c.id', 'b.student_id')
            ->select('c.name',
                    'c.lastname',
                    'b.consultant',
                    'b.client',
                    'a.quantity',
            DB::raw('COUNT(number_payment) AS last_payments'),DB::raw('SUM(quantity) as total'))
            ->where('a.status', '=', 0)
            ->where('a.number_payment', '=', 5)
            ->orWhere('a.number_payment', '=', 6)
            ->groupBy('c.name','c.lastname','b.consultant','b.client','a.quantity')
            ->get();
        return response()->json(['status' => true, 'data' => $getLastPayments],200);
    }

    public function lastThreeMonthsPayments()
    {
        $getLastThreePayments = DB::table('payments as a')
        ->join('information_payment as b', 'b.id', 'a.infoPayment_id')
        ->join('students as c', 'c.id', 'b.student_id')
        ->select('c.name',
                'c.lastname',
                'b.consultant',
                'b.client',
                'a.quantity',
        DB::raw('COUNT(number_payment) AS last_payments'),DB::raw('SUM(quantity) as total'))
        ->where('a.status', '=', 0)
        ->where('a.number_payment', '=', 10)
        ->orWhere('a.number_payment', '=', 11)
        ->orWhere('a.number_payment', '=', 12)
        ->groupBy('c.name','c.lastname','b.consultant','b.client','a.quantity')
        ->get();

        return response()->json(['status' => true, 'data' => $getLastThreePayments], 200);
    }

    public function paymentsComingSoon()
    {
        $getPaymentsComingSoon = DB::table('payments as a')
        ->join('information_payment as b', 'b.id', 'a.infoPayment_id')
        ->join('students as c', 'c.id', 'b.student_id')
        ->select('c.name',
                'c.lastname',
                'b.consultant',
                'a.date_payment')
        ->where('a.status', '=', 0)
        ->groupBy('c.name', 'c.lastname', 'b.consultant', 'a.date_payment')
        ->get();

        return response()->json(['status' => true, 'data' => $getPaymentsComingSoon], 200);
    }

    /**
     * !! Realizar función generica que reciba como parametro si la notificación fue leida
     */

    public function paymentsNotifications()
    {

        $paymentsNotifications = DB::table('payments as a')
        ->join('information_payment as b', 'b.id', 'a.infoPayment_id')
        ->join('students as c', 'c.id', 'b.student_id')
        ->select(
                'a.id',
                'c.name',
                'c.lastname',
                'b.consultant',
                'a.date_payment')
        ->where('a.status', '=', 0)
        ->where('a.is_read', '=', 0)
        ->groupBy('a.id','c.name', 'c.lastname', 'b.consultant', 'a.date_payment')
        ->get();

        return response()->json(['status' => true, 'data' => $paymentsNotifications], 200);
    }

    public function updateNotification($id)
    {
        $updateNotification = Payments::findOrFail($id);
        DB::beginTransaction();
        try {
            $updateNotification->is_read = 1;
            $updateNotification->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $updateNotification->is_read, 'message' => 'Notificación actualizada correctamente'],200);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ocurrio un error al actualizar las notificaciones'],500);
        }
    }


    public function accruedIncome()
    {
        $totalAccrued = DB::table('payments')->select(DB::raw('SUM(quantity) as total_acomulado'))->get();
        return response()->json(['status' => true, 'data' => $totalAccrued]);
    }

}
