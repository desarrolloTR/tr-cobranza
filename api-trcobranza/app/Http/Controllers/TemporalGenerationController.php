<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Carbon;
use App\Models\TemporalGeneration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TemporalGenerationRequest;

class TemporalGenerationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function registerGeneration(TemporalGenerationRequest $request) 
    {
        DB::beginTransaction();
        try {
            $generation = new TemporalGeneration;
            $generation->user_id = Auth::user()->id;
            $generation->name_generation = $request->input('name_generation');
            $generation->technology = $request->input('technology');
            $generation->created_at = Carbon::now();
            $generation->updated_at = null;
            $generation->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $generation, 'message' => 'Generación registrada correctamente'],200);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al registrar la generacion'. $e], 500);
        }
    }

    public function getGeneration($id) 
    {
        /**
         * Validamos si existen datos con el ID que pasamos como argumento
         *  en caso de que no encuentren los datos solicitados devuelve 404
         */
        $generation = TemporalGeneration::findOrFail($id);
        return response()->json(['status' => true, 'data' => $generation]);
    }

    public function getGenerations() 
    {

        /**
         * Devuelve toda las generaciones registradas
         */
        $generations = TemporalGeneration::all();
        return response()->json(['status' => true, 'data' => $generations]);
    }

    public function updateGeneration(TemporalGenerationRequest $request, $id) 
    {

        $generationUpdate = TemporalGeneration::findOrFail($id);
        DB::beginTransaction();
        try {
            $generationUpdate->name_generation = $request->input('name_generation');
            $generationUpdate->technology = $request->input('technology');
            $generationUpdate->status = $request->input('status');
            $generationUpdate->updated_at = Carbon::now();
            $generationUpdate->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $generationUpdate, 'message' => 'Datos actualizados correctamente'],201);
        } catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al actualizar los datos'], 500);
        }

    }

    /**
     * !!Devuelve las generaciones activas
     */

    public function getGenerationActive() 
    {
        $generationActive =  TemporalGeneration::where('status', 1)->get();
        return response()->json(['status' => true, 'data' => $generationActive]);
    }

    /**
     * !!Devuelve las generaciones que no estan activas
     */

    public function getGenerationInactive()
    {
        $generationInactive = TemporalGeneration::where('status', 2)->get();
        return response()->json(['status' => true, 'data' => $generationInactive]);
    }


}
