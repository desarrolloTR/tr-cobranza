<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Student;
use Illuminate\Support\Carbon;
use App\Models\TemporalGeneration;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StudentRequest;
use App\Models\Note;

class StudentController extends Controller
{
    public function registerStudent(StudentRequest $request)
    {
        DB::beginTransaction();
        try {
            $student = new Student;
            $student->generation_id = $request->input('generation_id');
            if(!$this->validateIfExistSomeGeneration($student->generation_id)) {
                return response()->json(['status' => false, 'message' => 'Error: Se esta asignando un estudiante a una generación que no existe, favor de verificar los datos'],404);
            }
            $student->name = $request->input('name');
            $student->lastname = $request->input('lastname');
            $student->email = $request->input('email');
            $student->telephone = $request->input('telephone');
            $student->telephone_optional = $request->input('telephone_optional');
            $student->created_at = Carbon::now();
            $student->updated_at = null;
            $student->status = 1;
            $student->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $student, 'message' => 'Estudiante registrado correctamente'], 200);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al registrar al usuario'], 500);
        }
    }

    public function getStudent($id)
    {
        $student = Student::findOrFail($id);
        return response()->json(['status' => true, 'data' => $student]);
    }

    public function getStudentForCategory($id)
    {
        $studentForCategory = DB::table('students as a')
            ->join('temporal_generation as b', 'b.id', 'a.generation_id')
            ->select('a.id as student_id',
            'a.name','a.lastname',
            'a.email','a.telephone',
            'a.telephone_optional',
            'a.status',
            'b.id as generation_id',
            'b.name_generation',
            'b.technology')
            ->where('a.id','=', $id)
            ->get();

        return response()->json(['status' => true, 'data' => $studentForCategory],200);
    }

    public function getStudentForComment($id)
    {
        $studentForNotes = DB::table('students as a')
        ->join('notes as b', 'a.id', 'b.student_id')
        ->select('b.id as note_id','a.id as student_id','b.notes')
        ->where('a.id', '=', $id)
        ->where('b.deleted_at','=', null)
        ->get();

        return response()->json(['status' => true, 'data' => $studentForNotes]);
    }

    public function getStudents()
    {
        $student = Student::orderBy('students.created_at','asc')
        ->join('temporal_generation', 'temporal_generation.id', '=', 'students.generation_id')
        ->select('students.id as student_id', 'students.name', 'students.lastname',
        DB::raw('CONCAT(students.name, " ", students.lastname) AS fullname'),
         'students.email',
         'students.telephone',
         'students.telephone_optional',
         'students.status',
         'students.created_at',
         'temporal_generation.user_id',
         'temporal_generation.name_generation',
         'temporal_generation.technology')
        ->get();

        if(count($student) == 0) {
            return response()->json(['status' => false, 'message' => 'No se encontraron regisitros']);
        } else {
            return response()->json(['status' => true, 'data' => $student]);
        }
    }


    public function getStudentsActive()
    {
        $studentActive = Student::where('status', 1)->get();
        return response()->json(['status' => true, 'data' => $studentActive]);
    }

    public function getStudentInactive()
    {
        $studenInactive = Student::where('status', 0)->get();
        return response()->json(['status' => true, 'data' => $studenInactive]);
    }

    
    public function updateStudent(StudentRequest $request, $id)
    {
        $student = Student::findOrFail($id);
        try {
            $student->generation_id = $request->input('generation_id');
            $student->name = $request->input('name');
            $student->lastname = $request->input('lastname');
            $student->email = $request->input('email');
            $student->telephone = $request->input('telephone');
            $student->telephone_optional = $request->input('telephone_optional');
            $student->status = $request->input('status');
            $student->updated_at = Carbon::now();
            $student->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $student, 'message' => 'Estudiante actualizado correctamente'], 200);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrio un error al actualizar el usuario', $e],500);
        }
    }

    private function validateIfExistSomeGeneration($generation){
        $getGeneration = TemporalGeneration::find($generation);
        return $getGeneration;
    }


}
