<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Note;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\NotesRequest;

class NotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function registerNote(NotesRequest $request) {
        DB::beginTransaction();
        try {
            $note = new Note;
            $note->student_id = $request->input('student_id');
            $note->notes = $request->input('notes');
            $note->created_at = Carbon::now();
            $note->updated_at = null;
            $note->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $note, 'message' => 'Nota creada correctamente'],201);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ha ocurrido un error al registrar su nota'], 500);
        }
    }

    public function getNotes() {
        $notes = Note::all();
        return response()->json(['status' => true, 'data' => $notes],200);
    }

    public function getNote($id) {
        $note = Note::findOrFail($id);
        return response()->json(['status' => true, 'data' => $note],200);
    }

    public function updateNote(NotesRequest $request, $id) {
        $note = Note::findOrFail($id);
        try {
            $note->user_id = $request->input('student_id');
            $note->notes = $request->input('notes');
            $note->updated_at = Carbon::now();
            $note->save();
            DB::commit();
            return response()->json(['status' => true, 'data' => $note, 'message' => 'La nota se ha actualizado correctamente'],201);
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => 'Ocurrio un error al actualizar la nota'], 500);
        }
    }

    public function deleteNotes($id)
    {
        $note = Note::findOrFail($id);
        $note->delete();
        return response()->json(['status' => true, 'message' => 'Nota eliminada correctamente']);
    }
}
