<?php

namespace App\Http\Requests;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Http\FormRequest;

class BankRequest extends FormRequest
{
    use SoftDeletes;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_bank' => 'required|unique:account_bank,account_bank,'.$this->id,
            'name' => 'required',
            'bank' => 'required',
            'clabe' => 'required|unique:account_bank,clabe,'.$this->id
        ];
    }

    public function messages()
    {
        return [
            'account_bank.required' => 'El campo número de cuenta es requerido',
            'name.required' => 'El campo nombre es requerido',
            'bank.required' => 'El campo banco es requerido',
            'clabe.required' => 'El campo clabe es requerido',
            'account_bank.unique' => 'La cuenta ya se encuentra registrada',
            'clabe.unique' => 'La clabe ya se encuentra registrada'
        ];
    }
}
