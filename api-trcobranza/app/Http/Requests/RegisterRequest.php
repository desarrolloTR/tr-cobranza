<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,id',
            'password' => 'required',
            'telephone' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es requerido',
            'email.required' => 'El campo email es requerido',
            'email.unique' => 'El email que esta ingresando ya existe',
            'password.required' => 'La contraseña es requerida',
            'telephone.required' => 'El teléfono es requerido',
            'telephone.max' => 'El teléfono debe de ser de 10 digitos',
        ];
    }
}
