<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case "POST" :
                return [
                    'date_payment' => 'required',
                    'quantity' => 'required',
                ];  
                break;
            case "PUT" :
                return [
                    'method_payment' => 'required', 
                    'status' => 'required'
                ];
                
                break;
            default:
                return response()->json('Algo salio mal');
        }
    }

    public function messages()
    {
        return [
            'number_payment.required' => 'El número de pago es requerido',
            'date_payment.required' => 'La fecha de pago es requerida',
            'quantity.required' => 'Es necesario espeficar una cantidad',
            'status.required' => 'El campo estatus de pago es requerido',
        ];
    }
}
