<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemporalGenerationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_generation' => 'required',
            'technology' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name_generation.required' => 'El nombre de la generación es requerido',
            'technology.required' => 'Es necesario escoger una tecnologia'
        ];
    }
}
