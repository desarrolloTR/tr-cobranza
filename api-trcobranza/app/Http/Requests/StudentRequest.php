<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class StudentRequest extends FormRequest
{

    private $defaultValidation = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'generation_id' => 'required',
            'name' => 'required',
            'lastname' => 'required',
            'telephone' =>  'required|max:10|min:10',
            'email' => 'unique:students,email,'.$this->id
        ];
    }

    public function messages()
    {
        return [
            'generation_id.required' => 'El campo generación es requerido',
            'name.required' => 'El campo nombre es requerido',
            'lastname.required' => 'El campo apellido es requerido',
            'email.required' => 'El campo email es requerido',
            'email.unique' => 'El correo electronico ya existe',
            'telephone.required' => 'El número de télefono es requerido',
            'telephone.min' => 'El número de télefono debe de contener 10 digitos'
        ];
    }
}
