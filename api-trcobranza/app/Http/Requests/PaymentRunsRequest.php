<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRunsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case "POST" :
                return [
                    'consultant' => 'required',
                    'client' => 'required',
                    'date_entry' => 'required',
                    'payment_period' => 'required',
                    'schema' => 'required',
                    'amount_receive' => 'required',
                ];
                break;
            case "PUT" :
                return [
                    'outstanding' => 'required', 
                ];
                
                break;
            default:
                return response()->json('Algo salio mal');
        }
    }
    public function messages()
    {
        return [
            'consultant.required' => 'La consultora es requerida',
            'client.required' => 'El cliente es requerido',
            'date_entry.required' => 'La fecha de inicio es requerido',
            'payment_period.required' => 'El periodo de pago es requerido',
            'schema.required' => 'El esquema de pago es requerido',
            'amount_receive.required' => 'El monto a recibir es requerido',
            'outstanding.required' => 'El restante es requerido'
        ];
    }
}
