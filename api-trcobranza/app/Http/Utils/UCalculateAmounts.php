<?php

namespace App\Http\Utils;

use DateInterval;
use DatePeriod;
use DateTime;

class UCalculateAmounts
{
    // Monto total a pagar del cliente
    public static function calculateTotalAmountPay($payrollAmount)
    {
        $fullCoursePayment = $payrollAmount * 2.5;
        return $fullCoursePayment;
    }

    public static function discountPorcentage(int $porsent, int $quantity)
    {
        $discount = ($quantity * $porsent) / 100;
        $finalAmount = $quantity - $discount;
        return $finalAmount;
    }

    public static function workedDays($startDate, $finishDate)
    {
        $dias = (strtotime($startDate) - strtotime($finishDate));
        $dias = abs($dias);
        $dias = floor($dias);
        return $dias;
    }
    // Calcular el pago que se deberá hacer por quincena
    public static function calculateUnitPaymentForFortnights(int $quantity) //12 quincenas
    {
        $PagoPorQuincena = $quantity / 12;
        return $PagoPorQuincena;
    }
    // Calcular el pago que se deberá hacer por mes
    public static function calculateUnitPaymentPerMonth(int $quantity)
    {
        $pagoPorMes = $quantity / 6;
        return $pagoPorMes;
    }

    public static function caculatePaymentDay(int $totalPAgoPorQuincena)
    {
        $totalPorDia = $totalPAgoPorQuincena / 15;
        return $totalPorDia;
    }
    // Genera las fechas en las que se deberán realizar los pagos por mes
    public static function generateDatePaymentByMonth(DateTime $fechaInicial)
    {
        $stringDate = $fechaInicial->format('d-m-Y');
        $fechaComoEntero = strtotime($stringDate);
        $mes = date("m", $fechaComoEntero);
        $dia = date("d", $fechaComoEntero);
        $numeroDias = "9";
        $cantidadDias = (int)$numeroDias;
        $diaDeFechaIngesada = (int)$dia;
        if ($diaDeFechaIngesada > $numeroDias) {
            $fechaInicialDeCorrida = $fechaInicial;
            $aumentarMes = 1;
            $fechaInicialDeCorrida->add(new \DateInterval("P{$aumentarMes}M"));
        } else {
            $fechaInicialDeCorrida = $fechaInicial;
        }
        $months = array();
        $period = new DatePeriod($fechaInicialDeCorrida, new DateInterval('P1M'), 5);
        foreach ($period as $fechaInicialDeCorrida) {
            $fechaInicialDeCorrida->modify('last day of this month');
            array_push($months, $fechaInicialDeCorrida->format('Y-m-d'));
        }
        return $months;
    }

    // Genera las fechas en las que se deberán realizar los pagos por quincena
    public static function generateDatePaymentByFortnights(Datetime $fechaInicial)
    {
        $stringDate = $fechaInicial->format('d-m-Y');
        $fechaComoEntero = strtotime($stringDate);
        $anio = date("Y", $fechaComoEntero);
        $mes = date("m", $fechaComoEntero);
        $dia = date("d", $fechaComoEntero);
        $numeroDias = "4";
        $cantidadDias = (int)$numeroDias;
        $diaDeFechaIngesada = (int)$dia;
        if ($diaDeFechaIngesada < $numeroDias) {
            $fechaInicialDeCorrida = $fechaInicial;
            $period = new DatePeriod($fechaInicialDeCorrida, new DateInterval('P1M'), 5);
            $fortNights = array();
            foreach ($period as $fechaInicialDeCorrida) {
                $fechaInicialDeCorrida->modify('last day of this month');
                array_push(
                    $fortNights,
                    $fechaInicialDeCorrida->format('Y-m-15'),
                    $fechaInicialDeCorrida->format('Y-m-d')
                );
            }
            return $fortNights;
        }
        if ($diaDeFechaIngesada >= $numeroDias && $diaDeFechaIngesada < 18) {
            $fechaInicialDeCorrida = $fechaInicial;
            $fortNights = array();
            $period = new DatePeriod($fechaInicialDeCorrida, new DateInterval('P1M'), 5);
            foreach ($period as $fechaInicialDeCorrida) {
                $fechaInicialDeCorrida->modify('last day of this month');
                array_push(
                    $fortNights,
                    $fechaInicialDeCorrida->format('Y-m-d'),
                    $fechaInicialDeCorrida->format('Y-m-15')
                );
            }
            return $fortNights;
        }
        if ($diaDeFechaIngesada >= 18) {
            $fortNights = array();
            $fechaInicialDeCorrida = $fechaInicial;
            $aumentarMes = 1;
            $fechaInicialDeCorrida->add(new \DateInterval("P{$aumentarMes}M"));
            $period = new DatePeriod($fechaInicialDeCorrida, new DateInterval('P1M'), 5);
            foreach ($period as $fechaInicialDeCorrida) {
                $fechaInicialDeCorrida->modify('last day of this month');
                array_push($fortNights, $fechaInicialDeCorrida->format('Y-m-15'), $fechaInicialDeCorrida->format('Y-m-d'));
            }
            return $fortNights;
        }
    }
}
