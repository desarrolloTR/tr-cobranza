<?php
namespace App\Http\Utils;

class UNumberRandoms
{
    public static function generateNumberRandom()
    {
        $number = mt_rand(10000, 99999);
        return $number;
    }
}
