<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentInformation extends Model
{
    use HasFactory;

    protected $table = "information_payment";
    protected $fillable = [
        'student_id',
        'consultant',
        'client',
        'date_entry',
        'payment_period',
        'schema',
        'amount_receive',
        'amount_payment',
        'outstanding'
    ];

    public function payment()
    {
        return $this->hasMany(Payments::class);
    }
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
