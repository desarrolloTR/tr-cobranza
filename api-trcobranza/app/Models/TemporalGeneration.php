<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class TemporalGeneration extends Model
{
    use HasFactory;

    protected $table = "temporal_generation";

    protected $fillable = [
        'user_id',
        'name_generation',
        'technology',
        'status'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    protected $cast = [
        'status' => 'string'
    ];
}
