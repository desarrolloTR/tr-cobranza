<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;
    protected $table = "account_bank";

    protected $fillable = [
        'account_bank',
        'name',
        'bank',
        'clabe'
    ];


}
