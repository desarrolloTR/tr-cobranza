<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;

    protected $fillable = [
        'infoPayment_id',
        'user_id',
        'number_payment',
        'date_payment',
        'quantity',
        'status',
        'method_payment',
        'discount',
        'discount_quantity',
        'description',
        'is_read'
    ];

    public function paymentInformation()
    {
        return $this->belongsTo(PaymentInformation::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
