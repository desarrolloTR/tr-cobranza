<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Note extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'student_id',
        'notes'
    ];

    public function student() {
        return $this->belongsTo(Student::class);
    }
}
