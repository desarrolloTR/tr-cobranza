<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Student extends Model
{
    use HasFactory, Notifiable;

    protected $table = "students";
    protected $fillable = [
        'generation_id',
        'name',
        'lastname',
        'email',
        'telephone',
        'status'
    ];

    public function category()
    {
        return $this->belongsTo(TemporalGeneration::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }
}
